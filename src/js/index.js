import '../css/style.scss';

const page = {
  data: {
    fields: {
      email: {
        identifier: 'email',
        rules: [
          {
            type: 'empty',
            prompt: '邮箱不能为空'
          },
          {
            type: 'regExp[/^([A-Za-z0-9_\\-\\.\*])+\@([A-Za-z0-9_\\-\\.])+\.([A-Za-z]{2,4})$/]',
            prompt: '邮箱格式有误'
          }
        ]
      },
      imageCode: {
        identifier: 'image-code',
        rules: [
          {
            type: 'regExp[/^\[A-Za-z0-9]{6}$/]',
            prompt: '请输入6位验证码'
          }
        ]
      },
      emailCode: {
        identifier: 'email-code',
        rules: [
          {
            type: 'regExp[/^\[A-Za-z0-9]{6}$/]',
            prompt: '请输入6位验证码'
          }
        ]
      },
      name: {
        identifier: 'name',
        rules: [
          {
            type: 'empty',
            prompt: '昵称不能为空'
          }
        ]
      },
      personalDomain: {
        identifier: 'personal-domain',
        rules: [
          {
            type: 'empty',
            prompt: '个性域名不能为空'
          },
          {
            type: 'regExp[/^[A-Za-z][A-Za-z0-9_\\-]{0,}[A-Za-z0-9]$/]',
            prompt: '个性域名格式有误'
          }
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type: 'regExp[/^.{6,}$/]',
            prompt: '密码至少六位'
          }
        ]
      },
      accountAgreement: {
        on: 'submit',
        identifier: 'account-agreement',
        rules: [
          {
            type: 'checked',
            prompt: '请勾选同意服务条款'
          }
        ]
      }
    }
  },

  init: function() {
    this.backUpVisible();
    this.setBinding();
  },

  backTop: function(duration) {
    const scrollStep = -window.scrollY / (duration / 10),
      backInterval = setInterval(function() {
      if (window.scrollY != 0) {
        window.scrollBy(0, scrollStep);
      } else {
        clearInterval(backInterval);
      }
    }, 10);
  },

  backUpVisible: function() {
    if ($(window).scrollTop() > 100) {
      $('.back-up').removeClass('invisible');
    } else {
      $('.back-up').addClass('invisible');
    }
  },

  setBinding: function() {
    $('.message .close').on('click', function() {
      $(this).closest('.message').transition('fade');
    });

    $('.register-wrapper form').form({
      on: 'blur',
      fields: this.data.fields,
      onValid: function() {
        $(this[0]).closest('.row.field').removeClass('invalid').addClass('valid');
      },
      onInvalid: function(field) {
        $(this[0]).closest('.row.field').removeClass('valid').addClass('invalid').next().html(field[0]);
      },
    });

    // verification code countdown
    $('#get-code').on('click', function() {
      event.preventDefault();
      if(!$('.register-wrapper form').form('is valid', 'email')) {
        return;
      }
      const $target = $(this);
      const initialText = $target.text();
      let intervalTimer;
      let time = 300;

      $target.attr('disabled', true).html(`${time--} 秒`);
      intervalTimer = setInterval(() => {
        $target.html(`${time--} 秒`);
        if (time < 0) {
          clearInterval(intervalTimer);
          $target.attr('disabled', false).html(initialText);
        }
      }, 1000);
    });

    // toggle passaword visible
    $('.toggle-visible').on('click', function() {
      const $target = $(this);
      const $input = $target.closest('.input-wrapper').find('input');
      $target.toggleClass('password-visible').toggleClass('password-invisible');

      const type = $target.hasClass('password-visible') ? 'password' : 'text';
      $input.attr('type', type);
    });

    $('.back-up').on('click', () => {
      this.backTop(300);
    })

    // todo: throttle function
    $(window).scroll(this.backUpVisible);
  }
};

page.init();
